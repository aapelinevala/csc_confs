#! /usr/bin/env bash
sudo bash -c "echo 'Acquire::http::proxy \"http://192.168.199.2:3128\";Acquire::ftp::proxy \"http://192.168.199.2:3128\";Acquire::https::proxy \"http://192.168.199.2:3128\";' > /etc/apt/apt.conf.d/95proxies";
sudo apt-get update;
sudo apt-get upgrade -y;
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/';
gpg --keyserver hkp://keyserver.ubuntu.com:80 --keyserver-options http-proxy=192.168.199.2:3128 --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9;
gpg -a --export E298A3A825C0D65DFD57CBB651716619E084DAB9 | sudo apt-key add -;
sudo apt-get install libcurl4-openssl-dev r-base jags -y;
sudo printf 'http_proxy=http://192.168.199.2:3128\nhttps_proxy=http://192.168.199.2:3128' > .Renviron;
sudo R -e 'install.packages(c("data.table", "rjags"), dependencies = TRUE)';
